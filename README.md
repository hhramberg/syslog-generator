# Syslog generator
Simple Python script that generates either [RFC3164](https://tools.ietf.org/html/rfc3164) or [RFC5424](https://tools.ietf.org/html/rfc5424) compliant syslog messages. 

## RFC5424 TLS
Note that TLS support for RFC5424 is not *yet* implemented. UDP remains the only supported transport protocol for both RFC3164 and RFC5424 messages.

## Prerequesites
- Python is the only prerequesite for this script. Developed on 3.9.3.

## Usage
Simple usage that generates a pre-defined RFC3164 syslog message.
```bash
python slog_generator.py
```
This will generate the message ```<189>Apr 19 13:34:01 client Example syslog message.``` which is sent to ```loalhost```. 

The below example takes a custom message and destination ip address.
```bash
python slog_generator.py -a 192.168.10.50 -m "I'm running low on hard disk capacity!"
```
This generates the message ```<189>Apr 19 13:36:21 client I'm running low on hard disk capacity!``` which is sent to ip address ```192.168.10.50```.

### Options
Supported options are listed below. Run the script with either ```--h``` or ```--help``` flag to see the same list in the terminal.

|Flag|Description|Range|Default|
|---|---|---|---|
|-a|Address of destination/recipient.|IPv4 or IPv6 address string.|localhost|
|-c|Count. Number of times to send message. If omitted the script will run until it is interrupted by the user.|Integer greater than 0|0|
|--facility|Sets syslog facility level of message to be sent.|Integer in range \[0, 23\]||
|-h|Host name of sender in syslog message.|Single word string.|client|
|--h, --help|Prints helpful information in the terminal.|||
|-i|Interval between messages being sent in milliseconds.|Integer greater than or equal to 10.|1000|
|-m|Message to send.|Double quoted string message.|"Example syslog message."|
|-p|Port number of destination/recipient.|Integer in range \[1, 65365\]|514|
|-s|Syslog standard to use.|Either RFC3164 or RFC5424|RFC3164|
|--severity|Sets syslog severity level of message to be sent.|Integer in range \[0, 7\]|5|
|-v, --v, --version|Prints the script version.|||
