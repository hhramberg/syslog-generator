import datetime
import ipaddress
import os
import sys
import socket
import sys
from time import sleep

APP_NAME = "Syslog Generator"
APP_VERSION = "1.0"
PORT_DEFAULT_TLS = 6514
PORT_DEFAULT = 514
STANDARDS = ["RFC3164", "RFC5424", "RFC5424_TLS"]  # RFC5424_TLS is not supported yet!


class Config:
    def __init__(self):
        self.Standard = 0
        self.Dst = ipaddress.ip_address("127.0.0.1")
        self.Port = PORT_DEFAULT
        self.Interval = 1000
        self.Count = 0
        self.Message = "Example syslog message."
        self.Facility = 23
        self.Severity = 5
        self.Host = "client"

    def string(self):
        print(
            "Standard:",
            self.Standard,
            "\nDst:",
            self.Dst,
            "\nPort:",
            self.Port,
            "\nInterval:",
            self.Interval,
            "\nCount:",
            self.Count,
            "\nMessage:",
            self.Message,
            "\nFacility:",
            self.Facility,
            "\nSeverity:",
            self.Severity,
        )


# Prints helpful information about the script and how to run it to std out.
def print_help():
    print(APP_NAME, APP_VERSION)
    print("Usage: python slog_generator.py [OPTION [ARGUMENT]] ... ")
    print(
        "Generates RFC3164 or RFC5424 syslog messages and sends them ro a recieving syslog server over UDP."
    )
    print(
        "\nArguments:\n\n"
        + "-a\t\t-\tIPv4 or IPv6 address string of destination syslog server. Default is localhost.\n"
        + "-c\t\t-\tNumber of times to send message. Integer value greater than 0. "
        + "If this option is omitted script will loop until interrupted. Default is 0 (send until interrupted).\n"
        + "--facility\t-\tSyslog facility. Integer value in range [0, 23]. Default is 23.\n"
        + "-h\t\t-\tName of sending host in syslog message.\n\n"
        + "--h\t\t-\tPrint this help message.\n"
        + "--help\n\n"
        + "-i\t\t-\tInterval between messages being sent in milliseconds. "
        + "Integer value greater than or equal to 10. Default is 1000 milliseconds.\n"
        + '-m\t\t-\tMessage to send. Use double quotes (") to encapsulate strings with spaces and other delimeters. '
        + 'Default is "Example syslog message."\n'
        + "-p\t\t-\tPort number of destination syslog server. Integer value in range [1, 65535]. Default depends on standard.\n"
        + "-s\t\t-\tSyslog standard. Currently supported standards are: {RFC3164, RFC5424}. Default is RFC3164.\n"
        + "--severity\t-\tSyslog severity. integer value in range [0, 7]. Default is 5.\n\n"
        + "-v\t\t-\tPrints application version.\n"
        + "--v\n"
        + "--version\n"
    )
    print(
        "No arguments are required to run the script, all flags have default options. However it will be very likely to use the -a "
        + "flag to tell the script the destination syslog server's IP address."
    )


# Parses the argument to a command line option.
def parse_arg(i1):
    if (i1 + 1 == len(sys.argv) and sys.argv[i1].startswith("-")) or sys.argv[
        i1 + 1
    ].startswith("-"):
        print("Error: got option", sys.argv[i1], "but no argument! Exiting...")
        exit(1)
    else:
        return sys.argv[i1 + 1]


# Parses command line options.
def parse_args():
    conf = Config()
    if len(sys.argv) > 1:
        for i1 in range(1, len(sys.argv)):
            if not sys.argv[i1].startswith("-"):
                continue

            if sys.argv[i1] == "-a":
                # Address: IP address of server/recipient.
                arg = parse_arg(i1)
                try:
                    ip = ipaddress.ip_address(arg)
                    conf.Dst = ip
                except ValueError:
                    print(
                        "Error: -a expected a valid IPv4 or IPv6 address, but got", arg
                    )
                    print("reverting to default address", conf.Dst)
            elif sys.argv[i1] == "-c":
                # Count: number of messages to be sent. Continues forever if omitted.
                arg = parse_arg(i1)
                try:
                    arg = int(arg)
                    conf.Count = arg
                except ValueError:
                    conf.Count = 1
                    print("Error: -c expected an integer argument, but got", arg)
                    print("setting count to default: 1")
            elif sys.argv[i1] == "--facility":
                # Facility: Facility of messages to be sent.
                arg = parse_arg(i1)
                try:
                    arg = int(arg)
                    if arg >= 0 and arg < 24:
                        conf.Facility = arg
                    else:
                        raise ValueError
                except ValueError:
                    print(
                        "Error: --facility expected an integer argument in the range [0, 23], but got",
                        arg,
                    )
                    print("reverting to default facility", conf.Facility)
            elif sys.argv[i1] == "-h":
                # Host: name of sender.
                arg = parse_arg(i1)
                conf.Host = arg
            elif sys.argv[i1] == "--h" or sys.argv[i1] == "--help":
                # Help: print help message.
                print_help()
                sys.exit(0)
            elif sys.argv[i1] == "-i":
                # Interval: time to wait before sending new syslog message in milliseconds.
                arg = parse_arg(i1)
                try:
                    arg = int(arg)
                    if arg >= 10:
                        conf.Severity = arg
                    else:
                        raise ValueError
                except ValueError:
                    print("Error: -i expected an integer argument, but got", arg)
                    print(
                        "reverting to default interval", conf.Interval, "milliseconds"
                    )
            elif sys.argv[i1] == "-m":
                # Message: Message string contained in double quotes. ("message").
                arg = parse_arg(i1)
                conf.Message = arg
            elif sys.argv[i1] == "-p":
                # Port: layer 4 (UDP or TCP) port of server/recipient.
                arg = parse_arg(i1)
                try:
                    arg = int(arg)
                    if arg > 0 and arg < 65536:
                        conf.Port = arg
                    else:
                        raise ValueError
                except ValueError:
                    print("Error: -p expected an integer argument, but got", arg)
                    print("reverting to default port", conf.Port)
            elif sys.argv[i1] == "-s":
                # Standard: {RFC3164, RFC5424, RFC5424_TLS}
                arg = parse_arg(i1)
                if arg == STANDARDS[0]:
                    conf.Standard = 0
                elif arg == STANDARDS[1]:
                    conf.Standard = 1
                elif arg == STANDARDS[2]:
                    conf.Standard = 2
                else:
                    print("Error: -s expected a Syslog standard, but got", arg)
                    print("reverting to default standard", STANDARDS[0])
            elif sys.argv[i1] == "--severity":
                # Severity: Severity of messages to be sent.
                arg = parse_arg(i1)
                try:
                    arg = int(arg)
                    if arg >= 0 and arg < 8:
                        conf.Severity = arg
                    else:
                        raise ValueError
                except ValueError:
                    print(
                        "Error: --severity expected an integer argument in the range [0, 7], but got",
                        arg,
                    )
                    print("reverting to default severity", conf.Severity)
            elif (
                sys.argv[i1] == "-v"
                or sys.argv[i1] == "--v"
                or sys.argv[i1] == "--version"
            ):
                # Version: print version message.
                print(APP_NAME, APP_VERSION)
                sys.exit(0)
            else:
                print("Error: unexpected argument:", sys.argv[i1])
                print("Exiting...")
                sys.exit(1)
    return conf


# Generates a synthetic syslog message.
def generate_syslog_message(conf):
    prival = str((conf.Facility << 3) + conf.Severity)
    if conf.Standard > 0:
        # RFC5424
        version = str(1)
        timestamp = datetime.datetime.utcnow().isoformat("T") + "Z"
        return f"<{prival}>1 {timestamp} {conf.Host} slog_generator {os.getpid()} generated_message - {conf.Message}"
    else:
        # RFC3164
        timestamp = datetime.datetime.utcnow().strftime("%b %d %H:%M:%S")
        return f"<{prival}> {timestamp} {conf.Host} {conf.Message}"


# Creates a socket for sending messages.
def create_socket(conf):
    if conf.Standard < 2:
        ver = socket.AF_INET
        if conf.Dst.version == 6:
            ver = socket.AF_INET6
        return socket.socket(ver, socket.SOCK_DGRAM)
    elif conf.Standard == 2:
        print("RFC5424 over TLS is currently not supported.")
        sys.exit(2)


# Loop script for conf.Count times or until user interrupts using CTRL-C or DELETE.
def run(conf):
    message = generate_syslog_message(conf)
    socket = create_socket(conf)
    if conf.Count > 0:
        c = 0
        print(
            "Sending message '"
            + message
            + "' every "
            + str(conf.Interval)
            + " miliseconds",
            conf.Count,
            "times to server ",
        )
        try:
            while c < conf.Count:
                c += 1
                socket.sendto(message.encode(), (str(conf.Dst), conf.Port))
                sleep(conf.Interval / 1000)
        except KeyboardInterrupt as ki:
            print(ki)
            print("User interrupt. Exiting...")
            sys.exit(0)
        except Exception as e:
            print(
                "Error: Failed to send message to "
                + str(conf.Dst)
                + ":"
                + str(conf.Port)
            )
            print(e)
    else:
        print(
            "Sending message '"
            + message
            + "' every "
            + str(conf.Interval)
            + " miliseconds until interrupted."
        )
        try:
            while True:
                socket.sendto(message.encode(), (str(conf.Dst), conf.Port))
                sleep(conf.Interval / 1000)
        except KeyboardInterrupt as ki:
            print(ki)
            print("User interrupt. Exiting...")
            sys.exit(0)
        except Exception as e:
            print(
                "Error: Failed to send message to "
                + str(conf.Dst)
                + ":"
                + str(conf.Port)
            )
            print(e)


if __name__ == "__main__":
    conf = parse_args()
    run(conf)
